"use strict";

exports.commands = {
	tcg: {

		//Give card to user
		give: function(target, room, user) {
			if (!user.can("rangeban")) return false;
		},

		//Take card from user
		take: function(target, room, user) {
			if (!user.can("rangeban")) return false;
		},

		//Remove all of a users cards
		takeall: function(target, room, user) {
			if (!user.can("rangeban")) return false;
		},

		//Transfer a users cards to a new userid
		transferall: function(target, room, user) {
			if (!user.can("rangeban")) return false;
		},

		//Enable/Disable TCG commands
		enable: "disable",
		disable: function(target, room, user) {
			if (!user.can("hotpatch")) return false;
		},

		//Ban user from gaining cards
		ban: function(target, room, user) {
			if (!user.can("rangeban")) return false;
			if (!target) return this.errorReply("You must specify a user.");
			target = Chat.escapeHTML(toId(target)).trim();
			const results = UserCards.ban(target);
			if (results) {
				this.sendReply(`${Spectra.nameColor(target, true)} has been banned from owning Cards.`);
			} else {
				this.sendReply(`Error banning ${Spectra.nameColor(target, true)}.`);
			}
		},

		//Unban a user from gaining cards
		unban: function(target, room, user) {
			if (!user.can("rangeban")) return false;
			if (!target) return this.errorReply("You must specify a user.");
			target = Chat.escapeHTML(toId(target)).trim();
			const results = UserCards.unban(target);
			if (results) {
				this.sendReply(`${Spectra.nameColor(target, true)} has been unbanned from owning Cards.`);
			} else {
				this.sendReply(`Error unbanning ${Spectra.nameColor(target, true)}.`);
			}
		}
	},

	showcase: function(target, room, user) {
		if (!this.runBroadcast()) return;
		if (!target) target = user.name;
		const targetUser = Chat.escapeHTML(toId(target).trim());
		const cards = UserCards.get(targetUser);
		if (!cards) {
			this.sendReplyBox(`${Spectra.nameColor(target, true)} has no Cards.`);
			return room.update();
		}
		if (cards === "banned") {
			this.sendReplyBox(`${Spectra.nameColor(target, true)} is banned from owning Cards.`);
			return room.update();
		}
		//TODO Add Card sorting

		//Build Output
		const html = []; //Array of card display HTML
		const invalid = []; //Invalid cards owned by a user
		const clean = []; //Remove these cards
		let num = 0; //Incremented on loop
		let iterator;
		let cardData; //Iteration variable
		for (const i in cards) {
			if (cards[i] === 0) {
				clean.push(i);
				continue;
			}
			cardData = TCG.getCard(i);
			if (!cardData) {
				invalid.push(i);
				continue;
			}
			for (iterator = 0; iterator < cards[i]; iterator++) {
				num++;
				html.push(`<button name="send" value="/card ${cardData.id}" class="card"><img src="${cardData.imageUrl}" width=80 height=111 title="${cardData.name}"></button>`);
			}
		}

		//Clean Database
		if (invalid.length >= 1) {
			for (const i of invalid) {
				Spectra.messageUpperStaff(`Invalid card ${i} deleted from ${targetUser}'s collection.`);
				TCG.clean(targetUser, i);
			}
		}
		if (clean.length >= 1) {
			for (const i of clean) {
				TCG.clean(targetUser, i);
			}
		}

		//Display
		if (html.length === 0) {
			this.sendReplyBox(`${Spectra.nameColor(target, true)} has no Cards.`);
			return room.update();
		}
		this.sendReplyBox(`<div style="max-height: 300px; overflow-y: auto>${html.join("")}</div><br><center>${Spectra.nameColor(target, true)} has ${num} Cards.</center>`);
 		return room.update();
 	},

/*
	card: function (target, room, user) {
		if (!target) return this.parse("/help card");
		const cardData = TCG.getCard(target);
		if (!cardData) return this.sendReply(`${id}: Card not found.`);

		let html = `<div class="card-display" id="card-div" ref="${cardData.id}">` + //Div open
				`<img src="${cardData.imageUrlHiRes}" height=220 title="${cardData.name}" align="right">` + //Image
				`<h1>${cardData.name}</h1>` + //Card Name
				`<b>Rarity: </b>${TCG.rarityDisplay(cardData.rarity)}<br>` + //Rarity
				`<b>Set: </b>${cardData.set}<br>` + //Set
				`<b>Series: </b>${cardData.series}<br>`; + //Series

		switch(toId(cardData.supertype)) {
		case "pokmon":
			html += `<div class="card-drop" id="card-extra-toggle-${cardData.id}"><center><h2>Detailed Information</h2></center></div>` + //Dropdown Div
					`<div class="card-extra no-disp" id="card-extra-${cardData.id}">`
					`<b>Card Type: </b>Pokémon<br>` + //Card Type
					`<b>Dex Number: </b>${cardData.nationalPokedexNumber}<br>` + //Dex Number
					`<b>Types: </b>${cardData.types.join("/")}<br>` + //Type
					`<b>SubType: </b>${cardData.subType}<br>` + //Subtype
					`${cardData.evolvesFrom ? `<b>Evolves From: </b>${cardData.evolvesFrom}<br>` : ""}` + //Evolves From
					`${cardData.retreatCost ? `<b>Retreat Cost: </b>${cardData.retreatCost.join(", ")}<br>` : ""}` + //Retreat Cost
					`<b>HP: </b>${cardData.hp}<br>`;//HP

			if (cardData.ability) {
				html += "<h2>Ability</h2>"; //Ability Header
				html += `<div class="card-subpanel">`; //Subpanel div
				html += `&nbsp;&nbsp;<b>Name: </b>${cardData.ability.name}<br>` + //Ability Name
						`&nbsp;&nbsp;<b>Type: </b>${cardData.ability.type}<br>` + //Ability Type
						`&nbsp;${cardData.ability.text}<br>`; //Ability Text
				html += "</div><br>"; //Close subpanel div
			}

			if (cardData.attacks) {
				html += "<h2>Attacks</h2>"; //Attack Header
				html += cardData.attacks.map(a => {
					let outp = `<div class="card-subpanel">`;
					outp += `&nbsp;&nbsp;<b>Name: </b>${a.name}<br>` + //Attack Name
							`&nbsp;&nbsp;<b>Damage: </b>${a.damage}<br>` + //Attack Damage
							`&nbsp;&nbsp;<b>Cost: </b>${a.cost.join(", ")} - (${a.convertedEnergyCost})<br>` + //Attack Cost
							`&nbsp;${a.text}<br>`;
					return outp += "</div>";
				}).join("<br>");

				html += "<br>";
			}

			if (cardData.resistances) {
				html += "<h3>Resistances</h3>";
				html += cardData.resistances.map(r => {
					return `<b>Type: </b>${r.type} - ${r.value}`;
				}).join("<br>");
				html += "<br>";
			}

			if (cardData.weaknesses) {
				html += "<h3>Weaknesses</h3>";
				html += cardData.weaknesses.map(w => {
					return `<b>Type: </b>${w.type} - ${w.value}`;
				}).join("<br>");
				html += "<br>";
			}

			html += "</div>"; //close card-extra div
		case "trainer":

		case "energy":
		}

		if (this.cmdToken === "!") {
			//Broadcast in room
			if (!this.runBroadcast()) return;
			this.sendReply(`|card|${html}`);
		} else {
			//Display card popup
			this.sendReply(`|popup||wide||card|${html}`);
		}
	},
	cardhelp: ["/card [name] - Displays information about a card."],
*/
};