"use strict";

exports.commands = {
	serverupdates: "news",
	announcements: "news",
	news: {
		staff: {
			'': 'view',
			show: 'view',
			view: function (target, room, user) {
				if (!this.runBroadcast()) return;
				if (!user.isStaff) return this.errorReply("/news staff - Access Denied");
				const output = News.getStaffNews(10);
				if (output) {
					this.sendReplyBox("<center><b>Staff News:</b></center>" + output.join("<hr>"));
				} else {
					this.sendReply("No Staff updates to display.");
				}
			},
			delete: function (target, room, user, connection) {
				if (Spectra.serverOwners.indexOf(user.userid) === -1) return this.errorReply("/news staff delete - Access Denied");
				if (!target) return this.parse('/help staff news');
				target = Chat.escapeHTML(toId(target).trim());

				const results = News.deleteStaffNews(target);
				if (typeof results === "string") return this.errorReply(results);
				this.privateModCommand(`(${user.name} deleted Staff announcement titled: ${target}.)`);
			},
			add: function (target, room, user, connection) {
				if (Spectra.serverOwners.indexOf(user.userid) === -1) return this.errorReply("/news staff add - Access Denied");
				if (!target) return this.parse('/help staff news');
				target = target.split('|');
				for (let u in target) target[u] = Chat.escapeHTML(target[u].trim());
				if (!target[1]) return this.errorReply("Usage: /news staff add Title| Message");

				const results = News.addStaffNews(target, user.name);
				if (typeof results === "string") return this.errorReply(results);
				this.privateModCommand(`(${user.name} added Staff announcement: ${target[1]})`);
			},
			help: function (target, room, user) {
				if (!this.runBroadcast()) return;
				if (!this.can('lock')) return;
				let output = "/news staff - Displays recent server updates. Requires % <br/>" +
					"/news staff add [Title] | [Content] - Adds update to list. Requires ~ <br/>" +
					"/news staff delete [Title] - Deletes update from recent updates. Requires ~ ";
				this.sendReplyBox(output);
			},
		},

		'': 'view',
		show: 'view',
		view: function (target, room, user) {
			if (!this.runBroadcast()) return;
			const output = News.getNews(10);
			if (output) {
				this.sendReplyBox("<center><b>Server News:</b></center>" + output.join("<hr>"));
			} else {
				this.sendReply("No Server updates to display.");
			}
		},

		delete: function (target, room, user, connection) {
			if (!this.can('rangeban')) return this.errorReply("/news delete - Access Denied");
			if (!target) return this.parse('/help news');
			target = Chat.escapeHTML(toId(target).trim());

			const results = News.deleteNews(target);
			if (typeof results === "string") return this.errorReply(results);
			this.privateModCommand(`(${user.name} deleted Server announcement titled: ${target}.)`);
		},

		add: function (target, room, user, connection) {
			if (!this.can('rangeban')) return this.errorReply("/news add - Access Denied");
			if (!target) return this.parse('/help news');
			target = target.split('|');
			for (let u in target) target[u] = Chat.escapeHTML(target[u].trim());
			if (!target[2]) return this.errorReply("Usage: /news add Title| Message| Type");
			if (["emergency", "development", "general", "community", "event"].indexOf(toId(target[2])) === -1) return this.errorReply("Please specify a valid update type: Development, Event, Community, General, Emergency");
			target[2] = target[2].charAt(0).toUpperCase() + toId(target[2].slice(1));

			const results = News.addNews(target);
			if (typeof results === "string") return this.errorReply(results);
			this.privateModCommand(`(${user.name} added Server announcement: ${target[1]})`);
		},

		toggle: function (target, room, user) {
			const bool = user.toggleNews();
			this.sendReply(`You have ${bool ? 'disabled' : 'enabled'} server updates.`);
		},

		help: function (target, room, user) {
			if (!this.runBroadcast()) return;
			let output = "/news - Displays recent server updates.<br/>/news toggle - Enables/Disables the displaying of updates on connection to Spectra.";
			if (this.can('rangeban')) {
				output += "<br/>/news add [Title] | [Content] | [Type] - Adds update to list.<br/>" +
				"/news delete [Title] - Deletes update from recent updates.";
			}
			this.sendReplyBox(output);
		},
	},
};
