"use strict";

//Used to catch user logins
Config.namefilter = function (name, user) {
	Spectra.updateSeen(toId(name));
	//Friends.notify(toId(name)); Unadded
	if (Spectra.customAvatars && Spectra.customAvatars[toId(name)]) {
		user.avatar = Spectra.customAvatars[toId(name)];
	}
	Spectra.getSymbol(toId(name), symbol => {
		user.rankDisplay = symbol ? symbol : null;
		if (user.updateIdentity) user.updateIdentity();
	});
	return name;
};
