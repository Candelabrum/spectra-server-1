"use strict";

/*

This Module is loaded on all processes and handles Console Logging,
Resource Monitoring, Crash Logging, Usage Reports

*/

const fs = require("fs");
const EventEmitter = require("events");

module.exports = class Monitor extends EventEmitter {
	constructor(pid) {
		super();
		this.pid = pid;
		this.spawned = Date.now();
		this.files = [
			"./logs/console.log",
			"./logs/resource.log",
			"./logs/crash.log",
			"./logs/usage.log",
		];
		this.streams = {};
	}

	init() {
		function createStream(i, callback) {
			let h = fs.createWriteStream(this.files[i], {"flags": "a"});

			h.on("open", () => {
				this.streams[i] = h;
				if (this.files.length - i > 1) {
					i++;
					createStream.call(this, i, callback);
				} else {
					this.files = null;
					return callback(false);
				}
			}).on("error", error => {
				return callback(error);
			});
		}

		createStream.call(this, 0, err => {
			if (err) {
				let processid = this.pid;
				console.log(`ProcessID: ${processid} | FATAL ERR LAUNCHING MONITOR | ${err}`);
				return;
			}
			this.status = "Ready";
			this.emit("ready", this);
		});

		return this;
	}

	write(index, date, msg) {
		this.streams[index].write(`\n${date} | ${msg}`);
	}

	log(msg, write) {
		let date = Date.now();
		let processid = this.pid;
		console.log(`ProcessID: ${processid} | ${Date.now()} | ${msg}`);
		if (write) {
			this.write.call(this, 0, date, msg);
		}
	}

	resource() {
		let date = Date.now();
		let processid = this.pid;
		let memUsage = process.memoryUsage();
		let results = [memUsage.rss, memUsage.heapUsed, memUsage.heapTotal];
		let units = ["B", "KB", "MB", "GB", "TB"];
		for (let i = 0; i < 3; i++) {
			let unitIndex = Math.floor(Math.log2(results[i]) / 10);
			results[i] = "" + (results[i] / Math.pow(2, 10 * unitIndex)).toFixed(2) + " " + units[unitIndex];
		}
		results = `ProcessID: ${processid} | ${Date.now()} | RESOURCE USAGE | RSS: ${results[0]}, Heap: ${results[1]} / ${results[2]}`;
		console.log(results);
		this.write.call(this, 1, date, results);
	}

	crash(msg, err, fatal) {
		let output = "";
		let processid = this.pid;
		if (msg) {
			output += msg;
			if (err) output += " | " + ((err.stack) ? err.stack : err);
		} else if (err) {
			output += ((err.stack) ? err.stack : err);
		} else {
			output = "NO CRASH DATA";
		}
		console.log(`ProcessID: ${processid} | ${Date.now()} | CRASH | ${output}`);
		this.write.call(this, 2, Date.now(), output);
		if (fatal) process.exit();
	}

	usage() {
		let usercount = Object.getOwnPropertyNames(Users.list).length, date = Date.now();
		console.log(`${date} | Connections: ${usercount}`);
		let log = `Connections: ${usercount} | `;
		for (let i in Users.list) log += `User: ${Users.list[i].userid}, Connection Time: ${Users.list[i].connectionTime} | `;
		this.write.call(this, 3, date, log);
	}

	status() {
		let processid = this.pid;
		let output = `ProcessID: ${processid} | STATUS | `;
		for (let i in loadedModules) output += `${loadedModules[i][0]} : ${loadedModules[i][2].status} | `;
		console.log(output);
	}
};
