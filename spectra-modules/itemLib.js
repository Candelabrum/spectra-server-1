"use strict";

module.exports = {
	avatartoken: {
		name: "Avatar Token",
		desc: "A token that, when used, will give the user a custom avatar. Usage: `/use avatartoken, image url`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/relicgold.png",
		tradeable: true,
		consumable: true,
		use: function (user, link, catcher) {
			if (!link) return this.sendReply("You must specify an Image");
			if (catcher) return this.sendReply("/use avatartoken, image url");
			Spectra.messageUpperStaff(`/raw <center><h4>Custom Avatar</h4>${user.name} has redeemed a Custom Avatar. <img src="${link}"><br><br><button name="send" value="/ca add ${user.userid},${link}">Apply</button> <button name="send" value="/rejectitem ${user.userid},avatar">Reject</button><br><br><hr></center>`);
			user.removeItem("avatartoken", 1);
			return this.sendReply("You have redeemed your Avatar Token, a member of staff will apply it shortly!");
		},
	},
	icontoken: {
		name: "Icon Token",
		desc: "A token that, when used, will give the user a userlist icon. Usage: `/use icontoken, imageurl`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/relicgold.png",
		tradeable: true,
		consumable: true,
		use: function (user, link, catcher) {
			if (!link) return this.sendReply("You must specify an Image");
			if (catcher) return this.sendReply("/use icontoken, image url");
			Spectra.messageUpperStaff(`/raw <center><h4>Userlist Icon</h4>${user.name} has redeemed a Userlist Icon. <img src="${link}"><br><br><button name="send" value="/icon set ${user.userid},${link}">Apply</button> <button name="send" value="/rejectitem ${user.userid},icon">Reject</button><br><br><hr></center>`);
			user.removeItem("icontoken", 1);
			return this.sendReply("You have redeemed your Icon Token, a member of staff will apply it shortly!");
		},
	},
	colortoken: {
		name: "Color Token",
		desc: "A token that, when used, will give the user access to a custom name color. Usage: `/use colortoken, colorhex`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/relicgold.png",
		tradeable: true,
		consumable: true,
		use: function (user, hex, catcher) {
			if (!hex || !hex.startsWith("#") || hex.length !== 7) return this.sendReply("You must specify a valid Hex. Example: #00B2EE");
			if (catcher) return this.sendReply("/use colortoken, color hex code");
			Spectra.messageUpperStaff(`/raw <center><h4>Custom Color</h4>${user.name} has redeemed a Custom Color. <font color="${hex}">${user.name}</font><br><br><button name="send" value="/color set ${user.userid},${hex}">Apply</button> <button name="send" value="/rejectitem ${user.userid},icon">Reject</button><br><br><hr></center>`);
			user.removeItem("colortoken", 1);
			return this.sendReply("You have redeemed your Color Token, a member of staff will apply it shortly!");
		},
	},
	musictoken: {
		name: "Music Token",
		desc: "A token that, when used, will give the user access to a custom battle music track. Usage: `/use musictoken, youtubelink`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/relicgold.png",
		tradeable: true,
		consumable: true,
		use: function () {
			return this.sendReply("This item is currently not fully supported.");
		},
	},
	symboltoken: {
		name: "Symbol Token",
		desc: "A token that, when used, will give the user access to a custom user symbol. Usage: `/use symboltoken, symbol`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/relicgold.png",
		tradeable: true,
		consumable: true,
		use: function (user, symbol, catcher) {
			if (!symbol) return this.sendReply("You must specify a 1 character symbol.");
			if (catcher) return this.sendReply("/use symboltoken, symbol");
			const bannedSymbols = ['!', '|', '‽', '\u2030', '\u534D', '\u5350', '\u223C'];
			for (let u in Config.groups) if (Config.groups[u].symbol) bannedSymbols.push(Config.groups[u].symbol);
			if (symbol.match(/([a-zA-Z ^0-9])/g) || bannedSymbols.indexOf(symbol) >= 0) return this.sendReply('This symbol is banned.');
			user.rankDisplay = symbol;
			user.updateIdentity();
			this.sendReply('Your symbol is now ' + symbol + '. You can remove it with /resetsymbol');
		},
	},
	fixtoken: {
		name: "Fix Token",
		desc: "A token that, when used, will allow the user to change their avatar, name color, icon, battle music, battle intro, or infobox. Usage: `/use fixtoken, thing, new value`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/relicgold.png",
		tradeable: true,
		consumable: true,
		use: function () {
			return this.sendReply("This item is currently not fully supported.");
		},
	},
	roomtoken: {
		name: "Room Token",
		desc: "A token that, when used, will allow the user to open an unofficial chatroom on the server. Usage: `/use roomtoken, roomname`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/relicgold.png",
		tradeable: true,
		consumable: true,
		use: function (user, name, catcher) {
			if (!name) return this.sendReply("You must specify a name.");
			if (catcher) return this.sendReply("/use roomtoken, name");
			Spectra.messageUpperStaff(`${user.username} has redeemed a room named ${name}.`);
			this.sendReply("You have redeemed your Room Token, a member of staff will assist you shortly!");
		},
	},
	infoboxtoken: {
		name: "Infobox Token",
		desc: "A token that, when used, will allow the user to have an infobox added to the server. You must provide your own HTML. Usage: `/use infoboxtoken`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/relicgold.png",
		tradeable: true,
		consumable: true,
		use: function (user, catcher) {
			if (catcher) return this.sendReply("/use infoboxtoken");
			Spectra.messageUpperStaff(`${user.username} has redeemed an Infobox.`);
			this.sendReply("You have redeemed your Infobox Token, a member of staff will assist you shortly; have the Infobox HTML ready!");
		},
	},
	battleintrotoken: {
		name: "Battle Intro Token",
		desc: "A token that, when used, will allow the user to have a custom battle intro added to the server. Usage: `/use battleintrotoken`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/relicgold.png",
		tradeable: true,
		consumable: true,
		use: function (user, catcher) {
			if (catcher) return this.sendReply("/use battleintrotoken");
			Spectra.messageUpperStaff(`${user.username} has redeemed a Battle Intro.`);
			this.sendReply("You have redeemed your Battle Intro Token, a member of staff will assist you shortly; have the Battle Intro HTML ready!");
		},
	},
	doublexpvoucher: {
		name: "Double XP Voucher",
		desc: "A voucher that, when used, will award you double XP for 24 hours. Usage: `/use doublexpvoucher`",
		img: "https://www.serebii.net/itemdex/sprites/pgl/spelltag.png",
		tradeable: true,
		consumable: true,
		use: function (user, catcher) {
			this.sendReply("This item is not currently Supported");
			if (catcher) return this.sendReply("/use doublexpvoucher");
			this.sendReply("You have redeemed your Double XP Voucher; it will remain in effect for 24 hours.");
		},
	},
	primebadge: {
		name: "Prime Staff Badge",
		desc: "A badge given to recognize former Prime staff members.",
		img: "https://cdn.bulbagarden.net/upload/thumb/2/28/Relic_Badge.png/662px-Relic_Badge.png",
		key: true,
		by: "Lights",
	},
	spectrabadge: {
		name: "Spectra Staff Badge",
		desc: "A badge given to recognize former and current Spectra staff members.",
		img: "https://cdn.bulbagarden.net/upload/b/b5/Rainbow_Badge.png",
		key: true,
		by: "Lights",
	},
	earlyadopterbadge: {
		name: "Early Adopter Badge",
		desc: "A badge given to recognize early members of Spectra.",
		img: "https://cdn.bulbagarden.net/upload/thumb/7/7d/Soul_Badge.png/50px-Soul_Badge.png",
		key: true,
		by: "Lights",
	},
};
