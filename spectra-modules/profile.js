"use strict";

const moment = require('moment');

module.exports = class {
	constructor() {
		this.clientEnabled = false;
		this.defaultPokes = [
			"victini",
			"totodile",
			"celebi",
			"gengar"
		];
	}

	init() {
		Spectra.UserClass.initFunctions.push(function(callback) {
			callback();
		});

		Spectra.UserClass.prototype.profileData = function(callback) {
			Spectra.UserClass.profileData(this.userid, data => {
				return callback(data);
			})
		};

		Spectra.UserClass.profileData = function(userid, callback) {
			database.all("SELECT * FROM profile WHERE userid=$userid", {
				$userid: userid,
			}, (err, results) => {
				let obj = {};
				if (err) {
					crash("Get Profile Data Error", err);
					obj.pTeam = [["victini","totodile","celebi","gengar"][Math.floor(Math.random() * 3)]];
					obj.pAbout = null;
					obj.pColors = [null,null,null];
					return callback(obj);
				} else if (results && results[0]) {
					let {about, color, color2, background, p1, p2, p3, p4, p5, p6} = results[0];
					obj.pTeam = [p1,p2,p3,p4,p5,p6];
					obj.pAbout = about || null;
					obj.pColors = [color ? color.split("|") : null, color2 ? color2.split("|") : null, background ? background.split("|") : null];
					return callback(obj);
				} else {
					obj.pTeam = [["victini","totodile","celebi","gengar"][Math.floor(Math.random() * 3)]];
					obj.pAbout = null;
					obj.pColors = [null,null,null];
					database.run("INSERT INTO profile(userid, p1) VALUES ($userid, $poke)", {
						$userid: userid,
						$poke: obj.pTeam[0]
					}, err => {
						if (err) crash("Init Profile Insert Error", err);
						return callback(obj);
					});
				}
			});
		}

		Spectra.UserClass.prototype.display = function(user) {
			Spectra.UserClass.display(user, this.userid);
		};

		Spectra.UserClass.display = function(caller, userid) {
			let html = "";
			Spectra.UserClass.getData(userid, 0, data => {

				//Constants
				const {
					avatar,
					symbol,
					group,
					regdate,
					seen,
					xp,
					title,
					friends,
					points,
					showcase,
					p
				} = data;
				const user = Users(userid,false,true);
				const username = user.name ? user.name : userid
				const lastOnline = user && user.connected ? "<font color=green style='text-shadow: 1px 1px 1px #000'>Currently Online</font>" : (seen ? moment(seen).format("MMMM Do YYYY, h:mm:ss A") + " EST. (" + (moment(seen).fromNow()) + ")" : "Never");
				const [r1,g1,b1] = p.pColors[0] ? p.pColors[0] : [0, 120, 200];
				const [r2,g2,b2] = p.pColors[1] ? p.pColors[1] : [234, 168, 89];
				const [br,bg,bb] = p.pColors[2] ? p.pColors[2] : [null,null,null];
				const label = function(str) {
					return `<font style="color: rgba(${r2},${g2},${b2}); text-shadow: 1px 1px 0 #000"><b>${str}: </b></font>`;
				}
				const panel = function(str, height = 300, hide) {
					return `<div style="border: 2px solid; border-radius: 5px; background: linear-gradient(to right,rgba(${r1},${g1},${b1}, 1),rgba(${r1},${g1},${b1}, .6)); max-height: ${height}; overflow-y: ${hide ? "hidden" : "auto"}">${str}</div>`;
				}

				//Main Header
				html += `<div style="${p.pColors[2] ? `background: rgba(${br}, ${bg}, ${bb}, 1); ` : ""}border: 2px solid; border-radius: 5px;">`;
				let headerHTML = `<img src="${avatar}" height=80 width=80 align=left>`;

				//Team
				if (p.pTeam) {
					headerHTML += '<div style="float: right; padding: 5px">';
					for (const id of p.pTeam) {
						if (!id) break;
						headerHTML += `<img src="${Spectra.getSprite(id)}">`;
					}
					headerHTML += "</div>";
				}

				//Minibox
				headerHTML += '<div style="float: left; width: 40%">';
				headerHTML += `&nbsp;${label("Name")}${Spectra.nameColor(username, true)}${title ? ` -${title}` : ""}<br>`;
				headerHTML += `&nbsp;${label("Registered")}<b><font color="#FFF" style="text-shadow: 1px 1px 0 #000">${regdate}</font></b><br>`;
				headerHTML += `&nbsp;${label("Rank")}<b><font color="#FFF" style="text-shadow: 1px 1px 0 #000">${symbol}${toId(group) === "regularuser" ? "" : group}</font></b><br>`;
				if (points) headerHTML += `&nbsp;${label("Points")}<b><font color="#FFF" style="text-shadow: 1px 1px 0 #000">${points}</font></b><br>`;
				headerHTML += `&nbsp;${label("Last Online")}<b>${lastOnline}</b>`;
				headerHTML += "</div>";

				html += panel(headerHTML + "<br>", 300, true);

				//About
				if (p.pAbout) html += "<br>" + panel(`<center><strong>About ${Spectra.nameColor(username, true)}:</strong><br><br>${Chat.escapeHTML(p.pAbout)}<br></center>`, 100) + "</br>";

				//Friends
				if (friends) {
					//Add Later
				}

				//XP
				const level = XP.getLevel(xp);
				const [remaining, percentage] = XP.getPercentage(xp);
				let c;
				switch(true) {
				case level < 5: c = "#FFFFFF"; break;
				case 10 > level >= 5: c = "#43cc3f"; break;
				case 20 > level >= 10: c = "#3f70cc"; break;
				case 25 > level >= 20: c = "#753fcc"; break;
				case 30 > level >= 25: c = "#cc3bc2"; break;
				case 35 > level >= 30: c = "#cc3b50"; break;
				case 40 > level >= 35: c = "#dd9446"; break;
				case level === 40: c = "#ddd146"; breal;
				}
				html += "<br>" + panel(
					`&nbsp;${label("Level")}<b><font color="${c}">${level}</font></b><br>` +
					`&nbsp;${label("Current XP")}<b>${xp}</b><br>` +
					`&nbsp;${label("XP to Next")}<b>${remaining}</b><br>` +
					`&nbsp;<div style="background-color: rgb(0,0,0); height: 10px; width: 400px; border: 2px solid; border-radius: 10px; margin-bottom: 0;"><div style="background: rgba(${r2},${g2},${b2},1); width: ${percentage}%; height: 8px; border: 1px solid; border-radius: 8px;"><center><b><font size=2 style="color: #FFF; text-shadow: 1px 1px 2px #000">${percentage}%</font></b></center></div></div><br>`
				);

				//Showcase
				if (showcase) {
					//Add Later
				}

				html += "</div>";
				caller.send("|popup||wide||html|" + html);
			});
		};

		Spectra.UserClass.prototype.displayMini = function(room, ctx, user) {
			Spectra.UserClass.displayMini(room, ctx, user, this.userid);
		};

		Spectra.UserClass.displayMini = function(room, ctx, caller, userid) {
			let html = "";
			Spectra.UserClass.getData(userid, 1, data => {

				//Constants
				const {
					avatar,
					symbol,
					group,
					regdate,
					seen,
					xp,
					title,
					points
				} = data;
				const user = Users(userid,false,true);
				const username = user.name ? user.name : userid;
				const lastOnline = user && user.connected ? "<font color=green style='text-shadow: 1px 1px 0 #000'>Currently Online</font>" : (seen ? moment(seen).format("MMMM Do YYYY, h:mm:ss A") + " EST. (" + (moment(seen).fromNow()) + ")" : "Never");
				const label = function(str) {
					return `<font style="color: #ff6600; text-shadow: 1px 1px 0 #000"><b>${str}: </b></font>`;
				}

				html += `<div style="padding-top: 10px; display: inline-block; width: 100%; background: linear-gradient(to right,rgba(0,120,200,1),rgba(0,120,200,.6))"><div style="float: left; border: 2p solid; height: 100px;"> <img src="${avatar}" height=80 width=80 align=left>`;
				html += `<div style="float: left;">&nbsp;${label("Name")}${Spectra.nameColor(username, true)}${title ? ` -${title}` : ""}<br>`;


				const level = XP.getLevel(xp);
				let c;
				switch(true) {
				case level < 5: c = "#FFFFFF"; break;
				case 10 > level >= 5: c = "#43cc3f"; break;
				case 20 > level >= 10: c = "#3f70cc"; break;
				case 25 > level >= 20: c = "#753fcc"; break;
				case 30 > level >= 25: c = "#cc3bc2"; break;
				case 35 > level >= 30: c = "#cc3b50"; break;
				case 40 > level >= 35: c = "#dd9446"; break;
				case level === 40: c = "#ddd146"; breal;
				}

				html += `&nbsp;${label("Level")}<b><font color="${c}" style="text-shadow: 1px 1px 0 #000">${level}</font></b><br>`;
				html += `&nbsp;${label("Registered")}<b><font color="#FFF" style="text-shadow: 1px 1px 0 #000">${regdate}</font></b><br>`;
				html += `&nbsp;${label("Rank")}<b><font color="#FFF" style="text-shadow: 1px 1px 0 #000">${symbol}${group}</font></b><br>`;
				if (points) html += `&nbsp;${label("Points")}<b><font color="#FFF" style="text-shadow: 1px 1px 0 #000">${points}</font></b><br>`;
				html += `&nbsp;${label("Last Online")}<b>${lastOnline}</b></div></div></div>`;
				ctx.sendReplyBox(html);
				room.update();
			});
		};

		Spectra.UserClass.prototype.editProfile = function(column, value) {
			Spectra.UserClass.editProfile(column, value, this.userid);
		};

		Spectra.UserClass.editProfile = function(column, value, userid) {
			database.run("UPDATE profile SET " + column + "=$value WHERE userid=$userid", {
				$userid: userid,
				$value: value || null
			}, (err) => {
				if (err) crash("Profile Edit Err", err);
			});
		};

		Spectra.UserClass.getData = function(userid, mini, callback) {
			const tasks = [];
			const user = Users(userid,false,true);

			tasks.push(new Promise((resolve, reject) => {
				resolve(Users.usergroups[userid] ? Users.usergroups[userid].substr(0, 1) : "Regular User");
			}));

			tasks.push(new Promise((resolve, reject) => {
				Spectra.regdate(userid, date => {
					resolve(date ? moment(date).format("MMMM DD, YYYY") : "Unregistered");
				});
			}));

			tasks.push(new Promise((resolve, reject) => {
				resolve(Spectra.customAvatars[userid] ? `https://spectra.reachinghalcyon.com:9600/avatars/${Spectra.customAvatars[userid]}` : "https://spectra.reachinghalcyon.com:9600/sprites/trainers/167.png");
			}));

			tasks.push(new Promise((resolve, reject) => {
				Spectra.lastSeen(userid, date => {
					resolve(date ? date : null);
				});
			}));

			tasks.push(new Promise((resolve, reject) => {
				user.getXp(xp => {
					resolve(xp || 0);
				}, userid);
			}));

			tasks.push(new Promise((resolve, reject) => {
				user.getCur({userid: userid, type: "points", callback: function(val) {
					resolve(val || 0);
				}});
			}));

			tasks.push(new Promise((resolve, reject) => {
				//Get Title
				resolve(null);
			}));

			if (mini === 0) {

				tasks.push(new Promise((resolve, reject) => {
					//Get Friends List
					resolve(null);
				}));

				tasks.push(new Promise((resolve, reject) => {
					//Get Card Showcase
					resolve(null);
				}));

				tasks.push(new Promise((resolve, reject) => {
					//Get Profile Data
					Spectra.UserClass.profileData(userid, p => {
						resolve(p);
					});
				}));
			}

			Promise.all(tasks).then((data) => {
				return callback({
					symbol: data[0],
					group: Config.groups[data[0]] ? Config.groups[data[0]].name : "Regular User",
					regdate: data[1],
					avatar: data[2],
					seen: data[3],
					xp: data[4],
					points: data[5],
					title: data[6],
					friends: data[7] || null,
					showcase: data[8] || null,
					p: data[9] || null
				});
			});
		}
	}
};