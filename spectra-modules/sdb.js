"use strict";

const sqlite3 = require("sqlite3");

module.exports = class extends sqlite3.Database {
	constructor() {
		super('./spectra-data/users.db');
	}

	init() {

		//Initialize Tables
		this.run("CREATE TABLE if not exists users (userid TEXT, lastSeen INTEGER, XP INTEGER, points INTEGER, news INTEGER, title TEXT, friendNotify INTEGER, rankDisplay TEXT)");
		this.run("CREATE TABLE if not exists friends (userid TEXT, friend TEXT)");
		this.run("CREATE TABLE if not exists profile (userid TEXT, about TEXT, color TEXT, color2 TEXT, background TEXT, p1 TEXT, p2 TEXT, p3 TEXT, p4 TEXT, p5 TEXT, p6 TEXT)");
		return this;
	}

	checkUser(userid, callback) {
		const tasks = [];

		tasks.push(new Promise((resolve, reject) => {
			this.all("SELECT * FROM users WHERE userid=$userid", {
				$userid: userid,
			}, (err, results) => {
				if (err) {
					resolve(["checkUser users Error", err]);
				} else if (results && results.length >= 1) {
					resolve(null);
				} else {
					this.run("INSERT INTO users(userid, lastSeen, XP, points, news, friendNotify) VALUES ($userid, 0, 0, 0, 1, 1)", {
						$userid: userid,
					}, err => {
						if (err) {
							resolve(["checkUser users Insert Error", err]);
						} else {
							resolve(null);
						}
					});
				}
			});
		}));

		tasks.push(new Promise((resolve, reject) => {
			this.all("SELECT * FROM friends WHERE userid=$userid", {
				$userid: userid,
			}, (err, results) => {
				if (err) {
					resolve(["checkUser friends Error", err]);
				} else if (results && results.length >= 1) {
					resolve(null);
				} else {
					this.run("INSERT INTO friends(userid) VALUES ($userid)", {
						$userid: userid,
					}, err => {
						if (err) {
							resolve(["checkUser friends Insert Error", err]);
						} else {
							resolve(null);
						}
					});
				}
			});
		}));

		Promise.all(tasks).then(errs => {
			for (let i = 0; i < errs.length; i++) {
				if (errs[i]) return callback(errs[i]);
			}

			return callback(null);
		});
	}
};
